import React from "react";
import "./App.css";
import TopBar from "./TopBar";
import { Router, Route, Redirect } from "react-router-dom";
import { createBrowserHistory as createHistory } from "history";
import ToastUI from "./toastui";
import CropperUI from "./cropper";
const history = createHistory();
function App() {
  return (
    <div className="App">
      <Router history={history}>
        <TopBar />
        <Route
          path="/toastui"
          component={ToastUI}
        />
        <Route
          path="/cropper"
          component={CropperUI}
        />
        <Redirect to="/toastui"></Redirect>
      </Router>
    </div>
  );
}
export default App;