import React, { useState, useEffect } from "react";
import "./HomePage.css";
import "tui-image-editor/dist/tui-image-editor.css";
import Button from "react-bootstrap/Button";

import rightImg from '../assets/right.png'
import wrongImg from '../assets/wrong.png'


const ImageEditor = require('tui-image-editor');
const download = require("downloadjs");

function HomePage() {
  const [isImageLoaded, setIsImageLoaded] = useState(false)
  const [imageEditor, setImageEditor] = useState(null)
  const [selectedObjectId, setSelectedObjectId] = useState(null)
  const [isCropActionBtnVisible, setIsCropActionBtnVisible] = useState(false)
  const canvasContainerRef = React.createRef()
  const [zoomLevel, setZoomLevel] = useState(1)
  useEffect(() => {
    const imageEditor = new ImageEditor(canvasContainerRef.current, {
      cssMaxWidth: 700,
      cssMaxHeight: 500,
      selectionStyle: {
        cornerSize: 20,
        rotatingPointOffset: 70,
      },
    });
    setImageEditor(imageEditor)

    imageEditor.on('objectActivated', function (props) {
      console.log(props);
      setSelectedObjectId(props.id)
    });
    console.log(isImageLoaded)
  }, [])

  const getObjectProperties = (id) => {
    return imageEditor.getObjectProperties(id, {
      angle: null,
      fill: null,
      height: null,
      id: null,
      left: null,
      opacity: null,
      stroke: null,
      strokeWidth: null,
      top: null,
      type: null,
      width: null,
    })
  }

  const getCanvasSize = () => {
    return imageEditor.getCanvasSize();
  }

  const setObjectProperties = (id, properties) => {
    imageEditor.setObjectProperties(id, properties)
  }

  const saveImageToDisk = () => {
    stopDrawingMode()


    const data = imageEditor.toDataURL();
    if (data) {
      const mimeType = data.split(";")[0];
      const extension = data.split(";")[0].split("/")[1];
      download(data, `image.${extension}`, mimeType);
    }
  };

  const addRightImg = () => {
    stopDrawingMode()

    const src = rightImg
    imageEditor.addImageObject(src).then(objectProps => {
      console.log(objectProps)
    });
  };

  const addWrongImg = () => {
    stopDrawingMode()

    const src = wrongImg
    imageEditor.addImageObject(src).then(objectProps => {
      console.log(objectProps)
    });
  };

  const addText = () => {
    stopDrawingMode()

    imageEditor.addText('Lorem Ipsum', {
      styles: {
        fill: '#000',
        fontSize: 100,
        fontWeight: 'bold'
      }
    }).then(objectProps => {
      console.log(objectProps.id);
    });
  };

  const addPen = () => {
    stopDrawingMode()

    imageEditor.startDrawingMode('FREE_DRAWING', {
      width: 10,
      color: 'rgba(255,0,0,0.5)'
    });
  };

  const addLine = () => {
    stopDrawingMode()

    imageEditor.startDrawingMode('LINE_DRAWING', {
      width: 10,
      color: 'rgba(255,0,0,0.5)'
    });
  };

  const addCircle = () => {
    stopDrawingMode()

    imageEditor.addShape('circle', {
      fill: 'transparent',
      stroke: 'red',
      strokeWidth: 3,
      rx: 100,
      ry: 50,
      isRegular: false
    }).then(objectProps => {
      console.log(objectProps.id);
    });
  };

  const startCropMode = () => {
    imageEditor.startDrawingMode('CROPPER', {
    });
    setIsCropActionBtnVisible(true)
  }

  const applyCropMode = () => {
    imageEditor.crop(imageEditor.getCropzoneRect());
    setIsCropActionBtnVisible(false)
  }

  const stopDrawingMode = () => {
    imageEditor.stopDrawingMode()
  }

  const rotate = () => {
    stopDrawingMode()

    imageEditor.rotate(90);
  };

  const deleteSelected = () => {
    if (!selectedObjectId) return
    imageEditor.removeObject(selectedObjectId)
    setSelectedObjectId(null)
  };

  const loadImage = (url) => {
    imageEditor.loadImageFromURL(url, 'lena').then(result => {
      setIsImageLoaded(true)
    });
  }

  function CropModeAction() {
    console.log("cropmodeaction")
    if (isCropActionBtnVisible && imageEditor && imageEditor.getDrawingMode() === 'CROPPER') {
      return <>
        <Button className='button' onClick={applyCropMode}>Apply</Button>
        <Button className='button' onClick={() => { stopDrawingMode(); setIsCropActionBtnVisible(false) }}>Cancel</Button>
      </>
    }

    return <></>
  }

  function LoadImageArray() {
    const urls = [
      "https://1.bp.blogspot.com/-CPsxwA6Jik8/Wrsk-Z3r6mI/AAAAAAAABCM/Z6sTq8zpNaYkwHk76jYXJNyD_2qJsID0wCEwYBhgL/s1600/20180328_102227-1.jpg",
      "https://thumbs.dreamstime.com/z/cors-caron-boardwalk-across-bog-near-tregaron-wales-62354242.jpg",
      "https://thumbs.dreamstime.com/z/golden-st-teilos-church-abergavenny-49379133.jpg",
      "https://media.gettyimages.com/photos/red-hot-air-balloons-over-jungle-nyaungu-mandalay-region-myanmar-picture-id977164032?s=2048x2048"
    ]

    return urls.map((url, index) =>
      <Button className='button' onClick={() => loadImage(url)} key={index}>{index + 1}</Button>
    )
  }

  const zoomIn = () => {
    const zoom = zoomLevel >= 5 ? 5 : zoomLevel + 0.5
    imageEditor.zoom({ x: 100, y: 100, zoomLevel:  zoom})
    setZoomLevel(zoom)
  }

  const zoomOut = () => {
    const zoom = zoomLevel <=1 ? 1 : zoomLevel - 0.5
    imageEditor.zoom({ x: 100, y: 100, zoomLevel:  zoom})
    setZoomLevel(zoom)
}

return (
  <div className="home-page">
    <div className="center">
      <LoadImageArray />
      <Button className='button' disabled={!isImageLoaded} onClick={saveImageToDisk}>Save</Button>
    </div>
    <div className="center">
      <Button className='button' disabled={!isImageLoaded} onClick={addRightImg}>Right</Button>
      <Button className='button' disabled={!isImageLoaded} onClick={addWrongImg}>Wrong</Button>
      <Button className='button' disabled={!isImageLoaded} onClick={addText}>Text</Button>
      <Button className='button' disabled={!isImageLoaded} onClick={addPen}>Pen</Button>
      <Button className='button' disabled={!isImageLoaded} onClick={addLine}>Line</Button>
      <Button className='button' disabled={!isImageLoaded} onClick={addCircle}>Circle</Button>
      <Button className='button' disabled={!isImageLoaded} onClick={deleteSelected}>Delete</Button>
      <Button className='button' disabled={!isImageLoaded} onClick={rotate}>Rotate</Button>
      <Button className='button' disabled={!isImageLoaded} onClick={startCropMode}>Crop</Button>
      <CropModeAction />
    </div>
    <div className="center">
      <Button className='button' disabled={!isImageLoaded} onClick={zoomIn}>Zoom In</Button>
      <Button className='button' disabled={!isImageLoaded} onClick={zoomOut}>Zoom Out</Button>
    </div>
    <div id="canvas-container" ref={canvasContainerRef}>
    </div>
  </div>
);
}
export default HomePage;