import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import './style.css'
import Cropper from 'cropperjs';

const IMG_SRC = 'https://media.gettyimages.com/photos/red-hot-air-balloons-over-jungle-nyaungu-mandalay-region-myanmar-picture-id977164032?s=2048x2048'

const Page = () => {
  const canvasContainerRef = React.createRef()
  const canvasRef = React.createRef()
  const [cropper, setStateCropper] = useState(null)

  const initializeCropper = () => {
    const cropper = new Cropper(canvasRef.current, {
      viewMode: 1,
      aspectRatio: 16 / 9
    });
    setStateCropper(cropper)
  }

  useEffect(() => {
  }, [])

  const onAddImage = () => {
    const canvas = canvasRef.current
    const context = canvas.getContext('2d')
    const img = new Image()
    img.crossOrigin = "anonymous";
    img.src = IMG_SRC
    img.onload = (event) => {
      canvas.width = img.width
      canvas.height = img.height
      // context.drawImage(img, 0, 0, img.width, img.height)
      context.drawImage(img, 0, 0)
      canvas.toDataURL()
      initializeCropper()
    }
  }

  return (
    <section id="cropper">
      <Button className='button' onClick={onAddImage}>Add Image</Button>
      <div id="canvas-container" ref={canvasContainerRef}>
        <canvas id="canvas" ref={canvasRef}>
          Your browser does not support the HTML5 canvas element.
        </canvas>
      </div>
    </section>
  );
}
export default Page;