# Image Editor POC with JS library - Toast UI Image Editor

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### Checkout out the libray at [ToastUI](https://nhn.github.io/tui.image-editor/latest/tutorial-example02-useApiDirect)

### Available Features

#### Insert Images, text, shapes, lines; Draw with pen

![All Options](screenshots/1.png)

#### Options in action
![Options demo](screenshots/2.png)

#### Zooming Functionality
![Zooming function](screenshots/3.png)

